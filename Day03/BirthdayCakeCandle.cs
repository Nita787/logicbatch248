﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class BirthdayCakeCandle
    {
        public static void Resolve ()
        {
            Console.WriteLine("Enter the set of number ");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);

            int maksimal = numbersArray.Max();
            int jumlahMaks = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] == maksimal)
                {
                    jumlahMaks++;
                }
            }

            Console.WriteLine("Jumlahnya adalah " + jumlahMaks);

            Console.ReadKey();
        }
    }
}
