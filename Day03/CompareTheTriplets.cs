﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class CompareTheTriplets
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the first set of number ");
            string numbers = Console.ReadLine();

            Console.WriteLine("Enter the second set of number ");
            string numbers2 = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);
            int[] numbersArray2 = Utility.ConvertStringToIntArray(numbers2);

            int barisPertama = 0;
            int barisKedua = 0;

            if (numbersArray.Length != numbersArray2.Length)
            {
                Console.WriteLine("Jumlah set number harus sama ");
            }
            else
            { 
                for (int i = 0; i < numbersArray.Length; i++)
                {

                    if (numbersArray[i] > numbersArray2[i])
                    {
                        barisPertama++;
                    }
                    else if (numbersArray[i] < numbersArray2[i])
                    {
                        barisKedua++;
                    }

                }
                Console.WriteLine("Jadi, perbandingannya adalah ");
                Console.Write(barisPertama + " " + barisKedua);
                Console.WriteLine();

            }
        }

    }
}
