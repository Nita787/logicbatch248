﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class PlusMinus
    {
        public static void Resolve ()
        {
           
            Console.WriteLine("Input the numbers ");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);

            int bilanganPositif = 0;
            int bilanganNegatif = 0;
            int bilanganNol = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] > 0)
                {
                    bilanganPositif += 1;
                }
                else if (numbersArray[i] < 0)
                {
                    bilanganNegatif += 1;
                }
                else
                {
                    bilanganNol += 1;
                }
            }
           

            double positif = Convert.ToDouble(bilanganPositif) / numbersArray.Length;
            double negatif = Convert.ToDouble(bilanganNegatif) / numbersArray.Length;
            double nol = Convert.ToDouble(bilanganNol) / numbersArray.Length;

            Console.WriteLine("Bilangan positif " + positif);
            Console.WriteLine("Bilangan negatif " + negatif);
            Console.WriteLine("Bilangan nol " + nol);
        }
    }
}
