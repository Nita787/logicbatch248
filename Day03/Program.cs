﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y") //answer to upper kalo user input huruf kecil akan diubah jadi kapital jd gak error
            {
                Console.WriteLine("Masukan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal) //untuk memilih apa inputan user
                {
                    case 1:
                        Console.WriteLine("Solve me first");
                        SolveMeFirst.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Time conversion");
                        TimeConversion.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Simple array sum");
                        SimpleArraySum.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Diagonal difference");
                        DiagonalDifference.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Plus minus");
                        PlusMinus.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Staircase ");
                        Staircase.Resolve();
                        break;

                    case 7:
                        Console.WriteLine("Mini-max sum ");
                        MiniMaxSum.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Birthday cake candle ");
                        BirthdayCakeCandle.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("A very big sum ");
                        VeryBigSum.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Compare The Triplets ");
                        CompareTheTriplets.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan ");
                        break;
                }
                
                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine();
            }
        }
        
    }
}
        