﻿using System;

namespace Day04
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y") //answer to upper kalo user input huruf kecil akan diubah jadi kapital jd gak error
            {
                Console.WriteLine("Masukan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal) //untuk memilih apa inputan user
                {
                    case 1:
                        Console.WriteLine("Camel case ");
                        CamelCase.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Strong password ");
                        StrongPassword.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Caesar chiper ");
                        CaesarChiper.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Mars exploration ");
                        MarsExploration.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Hackerrank in a string  ");
                        HackerrankInAString.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Pangrams ");
                        Pangrams.Resolve();
                        break;

                    case 8:
                        Console.WriteLine("Gem stone ");
                        GemStone.Resolve();
                        break;

                    case 9:
                        Console.WriteLine("Making anagrams ");
                        MakingAnagrams.Resolve();
                        break;

                    case 10:
                        Console.WriteLine("Two strings ");
                        TwoStrings.Resolve();
                        break;

                    case 11:
                        Console.WriteLine("Middle asterisk 3 ");
                        MiddleAsteriskTiga.Resolve();
                        break;

                    case 12:
                        Console.WriteLine("Palindrome ");
                        Palindrome.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan ");
                        break;
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine();
            }
        }

    }
}
