﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day04
{
    class StrongPassword
    {
        public static void Resolve ()
        {
            Console.WriteLine("Enter the password");
            string password = Console.ReadLine();

            char[] passwordChar = password.ToCharArray();

            int cekLower = 0;
            int cekUpper = 0;
            int cekSpec = 0;
            int cekNumber = 0;
            int total = 0;

            if (passwordChar.Length < 6)
            {
                Console.WriteLine("Minimal password is 6 digit, try again");
            }

            for (int i = 0; i < passwordChar.Length; i++)
            {
                if (char.IsLower(passwordChar[i]))
                {
                    cekLower += 1;
                }
                if (char.IsUpper(passwordChar[i]))
                {
                    cekUpper += 1;
                }
                if (!char.IsLetterOrDigit(passwordChar[i]))
                {
                    cekSpec += 1;
                }
                if (char.IsNumber(passwordChar[i]))
                {
                    cekNumber += 1;
                }
            }
            if (cekLower < 1)
            {
                total++;
            }
            if (cekUpper < 1)
            {
                total++;
            }
            if (cekSpec < 1)
            {
                total++;
            }
            if (cekNumber < 1)
            {
                total++;
            }

            Console.Write(total + " ");
            Console.WriteLine();
        }
    }
}
