﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day05
{
    class CustomSort
        //SOAL   : Urutkan angka dari yang terkecil ke terbesar
        //Input  : 2 1 3 5 2 1 4
        //Output : 1 1 2 2 3 4 5
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan angka yang akan diurutkan  ");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);

            int tukar;

            //Perulangan untuk geser angka terkecil ke paling kiri
            for (int i = 0; i < numbersArray.Length; i++)
            {
                //Perulangan untuk sorting dari angka terkecil -> terbesar
                for (int j = 1; j < numbersArray.Length; j++)
                {
                    //Kondisi untuk sorting descending
                    if (numbersArray[j] < numbersArray[j - 1])
                    {
                        tukar = numbersArray[j - 1];
                        numbersArray[j - 1] = numbersArray[j];
                        numbersArray[j] = tukar;
                    }
                }
            }
            //Perulangan untuk menampilkan hasil sorting
            for (int i = 0; i < numbersArray.Length; i++)
            {
                Console.Write(numbersArray[i] + " ");
            }
            Console.WriteLine();
        }
    }
}
