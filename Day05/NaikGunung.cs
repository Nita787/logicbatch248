﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day05
{
    class NaikGunung
    {
        public static void Resolve()
        {
            Console.WriteLine("Input U dan D tanpa spasi : ");
            string naikTurun = Console.ReadLine();

            char[] charArray = naikTurun.ToCharArray();

            int naikUp = 0;
            int turunDown = 0;
            int pendakian = 0;

            for (int i = 0; i < charArray.Length; i++)
            {
                if (charArray[i] == 'U' || charArray[i] == 'u')
                {
                    pendakian += 1;

                    if (pendakian == 1)
                    {
                        naikUp += 1;
                    }
                }
                else if (charArray[i] == 'D' || charArray[i] == 'd')
                {
                    pendakian -= 1;

                    if (pendakian == -1)
                    {
                        turunDown += 1;
                    }
                }
            }

            Console.WriteLine("Posisi Akhir : " + pendakian);
            Console.WriteLine("Jumlah naik :" + naikUp);
            Console.WriteLine("Jumlah turun :" + turunDown);
        }
    }
}
