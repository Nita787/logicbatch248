﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day05
{
    class Utility
    {
        public static int sum(int number1, int number2)
        {
            int total = number1 + number2;
            return total;
        }
        public static int SumTotal(int total1, int total2)
        {

            int total = total1 + total2;
            return total;
        }

        public static int[] ConvertStringToIntArray(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numbersArray = new int[stringNumbersArray.Length];

            //Convert to int
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = int.Parse(stringNumbersArray[i]);
            }


            return numbersArray;
        }

        public static int[] ConvertStringToIntArray1(string numbers)
        {
            string[] stringNumbersArray = numbers.Split(' ');
            int[] numbersArray = new int[stringNumbersArray.Length];

            //Convert to int
            for (int i = 0; i < numbersArray.Length; i++)
            {
                numbersArray[i] = int.Parse(stringNumbersArray[i]);
            }


            return numbersArray;
        }

        public static int[] ConvertStringToIntArray2(string numbers2)
        {
            string[] stringNumbersArray2 = numbers2.Split(' ');
            int[] numbersArray2 = new int[stringNumbersArray2.Length];

            //Convert to int
            for (int i = 0; i < numbersArray2.Length; i++)
            {
                numbersArray2[i] = int.Parse(stringNumbersArray2[i]);
            }


            return numbersArray2;
        
        }

    }
}
