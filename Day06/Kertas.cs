﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day06
{
    class Kertas
        //Soal  : Berapa banyak kertas A6 untuk kertas A(x) ?
    {
        public static void Resolve ()
        {
            Console.WriteLine("Pilihlah salah satu ukuran kertas di bawah ini ");
            Console.WriteLine("1. Kertas A0");
            Console.WriteLine("2. Kertas A1");
            Console.WriteLine("3. Kertas A2");
            Console.WriteLine("4. Kertas A3");
            Console.WriteLine("5. Kertas A4");
            Console.WriteLine("6. Kertas A5");

            int pilihan;

            Console.WriteLine("Masukan nomor pilihan ");
            pilihan = Convert.ToInt16(Console.ReadLine());

            double lebarA6 = 10.50;
            double panjangA6 = 14.80;
            double luasA6 = lebarA6 * panjangA6;
            
            switch (pilihan)
            {
                case 1:
                    Console.WriteLine("Kertas A0");
                    double lebarA0 = 84.10;
                    double panjangA0 = 118.90;
                    double luasA0 = lebarA0 * panjangA0;
                    double jumlahKertasA0 = luasA0 / luasA6;
                    int banyakKertasA0 = (int)jumlahKertasA0;

                    Console.WriteLine("Jumlah kertas A6 untuk kertas ukuran A0 adalah : " + banyakKertasA0 + " kertas");
                    break;

                case 2:
                    Console.WriteLine("Kertas A1");
                    double lebarA1 = 59.40;
                    double panjangA1 = 84.10;
                    double luasA1 = lebarA1 * panjangA1;
                    double jumlahKertasA1 = luasA1 / luasA6;
                    int banyakKertasA1 = (int)jumlahKertasA1;

                    Console.WriteLine("Jumlah kertas A6 untuk kertas ukuran A1 adalah : " + banyakKertasA1 + " kertas");
                    break;

                case 3:
                    Console.WriteLine("Kertas A2");
                    double lebarA2 = 42.00;
                    double panjangA2 = 59.40;
                    double luasA2 = lebarA2 * panjangA2;
                    double jumlahKertasA2 = luasA2 / luasA6;
                    int banyakKertasA2 = (int)jumlahKertasA2;

                    Console.WriteLine("Jumlah kertas A6 untuk kertas ukuran A2 adalah : " + banyakKertasA2 + " kertas");
                    break;

                case 4:
                    Console.WriteLine("Kertas A3");
                    double lebarA3 = 29.70;
                    double panjangA3 = 42.00;
                    double luasA3 = lebarA3 * panjangA3;
                    double jumlahKertasA3 = luasA3 / luasA6;
                    int banyakKertasA3 = (int)jumlahKertasA3;

                    Console.WriteLine("Jumlah kertas A6 untuk kertas ukuran A3 adalah : " + banyakKertasA3 + " kertas");
                    break;

                case 5:
                    Console.WriteLine("Kertas A4");
                    double lebarA4 = 21.00;
                    double panjangA4 = 29.70;
                    double luasA4 = lebarA4 * panjangA4;
                    double jumlahKertasA4 = luasA4 / luasA6;
                    int banyakKertasA4 = (int)jumlahKertasA4;

                    Console.WriteLine("Jumlah kertas A6 untuk kertas ukuran A4 adalah : " + banyakKertasA4 + " kertas");
                    break;

                case 6:
                    Console.WriteLine("Kertas A5");
                    double lebarA5 = 14.80;
                    double panjangA5 = 21.00;
                    double luasA5 = lebarA5 * panjangA5;
                    double jumlahKertasA5 = luasA5 / luasA6;
                    int banyakKertasA5 = (int)jumlahKertasA5;

                    Console.WriteLine("Jumlah kertas A6 untuk kertas ukuran A5 adalah : " + banyakKertasA5 + " kertas");
                    break;

                default:
                    Console.WriteLine("Inputan anda salah, masukan lagi ");
                    Console.ReadKey();

                break;
            }

        }
    }
}
