﻿using System;

namespace Day06
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";

            while (answer.ToUpper() == "Y") //answer to upper kalo user input huruf kecil akan diubah jadi kapital jd gak error
            {
                Console.WriteLine("Masukan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal) //untuk memilih apa inputan user
                {
                    case 1:
                        Console.WriteLine("Kertas ");
                        Kertas.Resolve();
                        break;

                    case 2:
                        Console.WriteLine("Urutan abjad ");
                        UrutanAbjad.Resolve();
                        break;

                    case 3:
                        Console.WriteLine("Tarif parkir ");
                        TarifParkir.Resolve();
                        break;

                    case 4:
                        Console.WriteLine("Belanja online ");
                        BelanjaOnline.Resolve();
                        break;

                    case 5:
                        Console.WriteLine("Set gambreng ");
                        SetGambreng.Resolve();
                        break;

                    default:
                        Console.WriteLine("Soal tidak ditemukan ");
                        break;
                }

                Console.WriteLine("Lanjutkan?");
                answer = Console.ReadLine();
            }
        }
    }
}
