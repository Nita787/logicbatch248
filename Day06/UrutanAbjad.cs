﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day06
{
    class UrutanAbjad
    {
        public static void Resolve ()
        {
            Console.WriteLine("Masukan kalimat ");
            string kalimat = Console.ReadLine();

            char[] kalimatArray = kalimat.ToLower().ToCharArray();

            Console.Write("Huruf vokal : ");
            for (int i = 0; i < kalimat.Length; i++)
            {

                if (kalimatArray[i] == 'A' || kalimatArray[i] == 'I' || kalimatArray[i] == 'U' || kalimatArray[i] == 'E' || kalimatArray[i] == 'O' ||
                    kalimatArray[i] == 'a' || kalimatArray[i] == 'i' || kalimatArray[i] == 'u' || kalimatArray[i] == 'e' || kalimatArray[i] == 'o')
                {
                    Array.Sort(kalimatArray);
                    Console.Write(kalimatArray[i]);
                }
            }
            Console.WriteLine();

            Console.Write("Huruf konsonan : ");
            for (int i = 0; i < kalimat.Length; i++)
            {
                if (kalimatArray[i] == 'B' || kalimatArray[i] == 'C' || kalimatArray[i] == 'D' || kalimatArray[i] == 'F' || 
                    kalimatArray[i] == 'G' || kalimatArray[i] == 'H' || kalimatArray[i] == 'J' || kalimatArray[i] == 'K' || kalimatArray[i] == 'L' ||
                    kalimatArray[i] == 'M' || kalimatArray[i] == 'N' || kalimatArray[i] == 'P' || kalimatArray[i] == 'Q' || kalimatArray[i] == 'R' ||
                    kalimatArray[i] == 'S' || kalimatArray[i] == 'T' || kalimatArray[i] == 'V' || kalimatArray[i] == 'W' || kalimatArray[i] == 'X' ||
                    kalimatArray[i] == 'Y' || kalimatArray[i] == 'Z' ||
                    kalimatArray[i] == 'b' || kalimatArray[i] == 'c' || kalimatArray[i] == 'd' || kalimatArray[i] == 'f' ||
                    kalimatArray[i] == 'g' || kalimatArray[i] == 'h' || kalimatArray[i] == 'j' || kalimatArray[i] == 'k' || kalimatArray[i] == 'l' ||
                    kalimatArray[i] == 'm' || kalimatArray[i] == 'n' || kalimatArray[i] == 'p' || kalimatArray[i] == 'q' || kalimatArray[i] == 'r' ||
                    kalimatArray[i] == 's' || kalimatArray[i] == 't' || kalimatArray[i] == 'v' || kalimatArray[i] == 'w' || kalimatArray[i] == 'x' ||
                    kalimatArray[i] == 'y' || kalimatArray[i] == 'z')
                {
                    Array.Sort(kalimatArray);
                    Console.Write(kalimatArray[i]);
                }
            }
            Console.WriteLine();
        }
    }
}
