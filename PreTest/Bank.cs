﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreTest
{
    class Bank
    {
        public static void Resolve()
        {
            string password = "123456";

            Console.WriteLine("Masukan pin anda ");
            string passwordUser = Console.ReadLine();

            if (passwordUser != password)
            {
                Console.WriteLine("Maaf pin yang anda masukan salah ");
            }
            else
            {
                Console.WriteLine("Masukan nominal uang yang ingin disetor ");
                int nominalUang = int.Parse(Console.ReadLine());

                Console.WriteLine("Pilihan Transfer : ");
                Console.Write("1. Antar Rekening      2. Antar Bank");
                Console.WriteLine();
                Console.WriteLine("Masukan pilihan dengan angka (misal 1 atau 2)");
                int pilihan = Convert.ToInt16(Console.ReadLine());


                switch (pilihan)
                {
                    case 1:
                        Console.WriteLine("Transfer antar rekening");
                        Console.WriteLine("---------------------------------------------------------------------");
                        Console.WriteLine("Masukan rekening tujuan");
                        string rekeningTujuan = Console.ReadLine();
                        Console.WriteLine("Masukan nominal uang yang ditransfer");
                        int nominalSetor = int.Parse(Console.ReadLine());
                        Console.WriteLine("----------------------------------------------------------------------");

                        if (nominalUang < nominalSetor)
                        {
                            Console.WriteLine("Maaf saldo anda tidak mencukupi");
                        }
                        else
                        {

                            int sisaSaldo = nominalUang - nominalSetor;
                            Console.WriteLine("Transaksi berhasil, saldo anda saat ini Rp. " + sisaSaldo + ",-");
                        }
                        break;

                    case 2:
                        Console.WriteLine("Transfer antar bank");
                        Console.WriteLine("---------------------------------------------------------------------");
                        Console.WriteLine("Masukan kode bank");
                        string kodeBank = Console.ReadLine();
                        Console.WriteLine("Masukan rekening tujuan");
                        string rekeningTujuanBank = Console.ReadLine();
                        Console.WriteLine("Masukan nominal uang yang ditransfer");
                        int nominalSetorBank = int.Parse(Console.ReadLine());
                        Console.WriteLine("---------------------------------------------------------------------");

                        if ((nominalUang - 7500 < nominalSetorBank))
                        {
                            Console.WriteLine("Maaf saldo anda tidak mencukupi");
                        }
                        else
                        {
                            int sisaSaldoBank = (nominalUang - nominalSetorBank) - 7500;
                            Console.WriteLine("Transaksi berhasil, saldo anda saat ini Rp. " + sisaSaldoBank + ",-");
                        }
                        break;
                }
            }
        }
    }
}
