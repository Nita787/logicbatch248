﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreTest
{
    class Bensin
    {
        public static void Resolve()
        {
            Console.WriteLine("Rute perjalanan : ");
            Console.WriteLine("1. Toko - Tempat 1 - Tempat 2 - Toko");
            Console.WriteLine("2. Toko - Tempat 1 - Tempat 2 - Tempat 3 - Tempat 4 - Toko");

            int pilihan;

            Console.WriteLine("Pilih salah satu dari rute yang disediakan di atas");
            pilihan = Convert.ToInt16(Console.ReadLine());

            double tokoKeTempatSatu = 2;
            double tempatSatuKeDua = 0.5;
            double tempatDuaKeTiga = 1.5;
            double tempatTigaKeEmpat = 2.5;
            double bensin = 2.5;

            switch (pilihan)
            {
                case 1:
                    Console.WriteLine("Pilihan rute : Toko - Tempat 1 - Tempat 2 - Toko");
                    double totalRutePertama = (tokoKeTempatSatu + tempatSatuKeDua) * 2;
                    double bensinPertama = totalRutePertama / bensin;
                    int rutePertama = (int)bensinPertama;
                    Console.WriteLine("Rute pertama memerlukan bensin sebanyak : " + rutePertama + " liter");
                    break;

                case 2:
                    Console.WriteLine("Pilihan rute : Toko - Tempat 1 - Tempat 2 - Tempat 3 - Tempat 4 - Toko");
                    double totalRuteKetiga = (tokoKeTempatSatu + tempatSatuKeDua + tempatDuaKeTiga + tempatTigaKeEmpat) * 2;
                    double bensinKetiga = totalRuteKetiga / bensin;
                    int ruteKetiga = (int)bensinKetiga;
                    Console.WriteLine("Rute pertama memerlukan bensin sebanyak : " + ruteKetiga + " liter");
                    break;
            }
        }
    }
}
