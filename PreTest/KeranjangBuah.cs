﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreTest
{
    class KeranjangBuah
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan banyak buah di keranjang 1 : ");
            int keranjangSatu = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukkan banyak buah di keranjang 2 : ");
            int keranjangDua = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukkan jumlah buah di keranjang 3 : ");
            int keranjangTiga = int.Parse(Console.ReadLine());

            int pilihan;

            Console.WriteLine("Pilih keranjang yang mau dibawa ke pasar");
            pilihan = Convert.ToInt16(Console.ReadLine());

            switch (pilihan)
            {
                case 1:
                    Console.WriteLine("Keranjang 1 dibawa kepasar");
                    int sisaBuahKeranjangSatu = keranjangDua + keranjangTiga;
                    Console.WriteLine("Jadi sisa buah di dapur ada sebanyak " + sisaBuahKeranjangSatu + " buah");
                    break;

                case 2:
                    Console.WriteLine("Keranjang 2 dibawa kepasar");
                    int sisaBuahKeranjangDua = keranjangSatu + keranjangTiga;
                    Console.WriteLine("Jadi sisa buah di dapur ada sebanyak " + sisaBuahKeranjangDua + " buah");
                    break;

                case 3:
                    Console.WriteLine("Keranjang 3 dibawa kepasar");
                    int sisaBuahKeranjangTiga = keranjangSatu + keranjangDua;
                    Console.WriteLine("Jadi sisa buah di dapur ada sebanyak " + sisaBuahKeranjangTiga + " buah");
                    break;

                default:
                    Console.WriteLine("Inputan anda salah, masukan lagi ");
                    Console.ReadKey();

                    break;
            }
        }
    }
}
