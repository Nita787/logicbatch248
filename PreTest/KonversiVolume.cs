﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreTest
{
    class KonversiVolume
    {
        public static void Resolve ()
        {
            Console.WriteLine("Pilihlah salah satu dari konversi di bawah ini ");
            Console.WriteLine("1. Konversi 1 botol ke cangkir");
            Console.WriteLine("2. Konversi 1 teko ke gelas");

            int pilihan;

            Console.WriteLine("Masukan nomor pilihan ");
            pilihan = Convert.ToInt16(Console.ReadLine());

            int botol;
            int teko;
            double gelas;

            switch (pilihan)
            {
                case 1:
                    Console.WriteLine("Konversi 1 botol ke cangkir ");
                    botol = 2; //2 gelas
                    gelas = 2.5;
                    double konversiBotol = botol * gelas;
                    int konversiBotolKeCangkir = (int)konversiBotol;

                    Console.WriteLine("Jadi 1 botol : " + konversiBotolKeCangkir + " cangkir");
                    break;

                case 3:
                    Console.WriteLine("Konversi 1 teko ke gelas ");
                    teko = 25; 
                    gelas = 2.5;
                    double konversiTeko = teko / gelas;
                    int konversiTekoKeGelas = (int)konversiTeko;

                    Console.WriteLine("Jadi 1 teko : " + konversiTekoKeGelas + " gelas");
                    break;

                default:
                    Console.WriteLine("Inputan anda salah, masukan lagi ");
                    Console.ReadKey();

                    break;
            }
        }
    }
}
