﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreTest
{
    class PoinPulsa
    {
        public static void Resolve ()
        {
            Console.WriteLine("Masukkan nominal pulsa : ");
            int nominalPulsa = int.Parse(Console.ReadLine());

            int point = 0;

            if (nominalPulsa <= 10000)
            {
                point = 0;
            }
            else if (nominalPulsa >= 10001 && nominalPulsa <= 30000)
            {
                point = ((nominalPulsa - 10000) / 1000) * 1;
            }
            else if (nominalPulsa > 30000)
            {
                point = ((nominalPulsa - 20000) / 1000) * 2;
            }
            Console.WriteLine("Poin yang kamu dapatkan adalah " + point + " poin");
        }
    }
}
