﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PreTest
{
    class PorsiMakan
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan jumlah laki-laki dewasa ");
            int jumlahLakiDewasa = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan jumlah perempuan dewasa ");
            int jumlahPerempuanDewasa = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukan jumlah balita ");
            int jumlahBalita = int.Parse(Console.ReadLine());

            int totalLakiDewasa = jumlahLakiDewasa * 2;
            int totalPerempuanDewasa = jumlahPerempuanDewasa * 1;
            int totalBalita = jumlahBalita * 1;

            int totalPorsiMakan = totalLakiDewasa + totalPerempuanDewasa + totalBalita;
            Console.WriteLine("Total porsi makan : " + totalPorsiMakan + " porsi");
        }
    }
}
