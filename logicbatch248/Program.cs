﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01
{
    class Program
    {
        static void Main(string[] args) //method atau function 
        {

            Console.WriteLine("Input nomor soal");
            int nomorSoal = int.Parse(Console.ReadLine());

            if (nomorSoal == 1)
            {
                //Soal BMI
                Console.WriteLine("Masukan tinggi dalam satuan meter");
                double tinggi = double.Parse(Console.ReadLine());

                Console.WriteLine("Masukan berat dalam satuan kg");
                double berat = double.Parse(Console.ReadLine());

                double BMI = berat / (tinggi * tinggi);

                if (BMI < 18.5)
                {
                    Console.WriteLine("Underweight");
                }
                else if (BMI > 25)
                {
                    Console.WriteLine("Overweight");
                }
                else
                {
                    Console.WriteLine("Normal");
                }
            }
            
            else if (nomorSoal == 2)
            {
                //Soal LMS

                Console.WriteLine("Masukan panjang angka");
                int length = int.Parse(Console.ReadLine());

                //Soal 1
                int[] angkaGanjilArray = new int[length];
                int angkaGanjil = 1;

                //memasukan value
                for (int i = 0; i < length; i++) // i = indeks
                {
                    angkaGanjilArray[i] = angkaGanjil;
                    angkaGanjil += 2;
                }

                //cetak value
                for (int i = 0; i < length; i++)
                {
                    Console.Write(angkaGanjilArray[i] + " ");
                }

                Console.WriteLine();


                //Soal 2
                int[] angkaGenapArray = new int[length];
                int angkaGenap = 2;

                for (int i = 0; i < length; i++)
                {
                    angkaGenapArray[i] = angkaGenap;
                    angkaGenap += 2;
                    Console.Write(angkaGenapArray[i] + " ");
                }

                Console.WriteLine();


                //Soal 3
                int[] kelipatanTigaArray = new int[length];
                int kelipatanTiga = 1;

                for (int i = 0; i < length; i++)
                {
                    kelipatanTigaArray[i] = kelipatanTiga;
                    kelipatanTiga += 3;
                    Console.Write(kelipatanTigaArray[i] + " ");
                }
                Console.WriteLine();


                //Soal 4
                int[] kelipatanEmpatArray = new int[length];
                int kelipatanEmpat = 1;

                for (int i = 0; i < length; i++)
                {
                    kelipatanEmpatArray[i] = kelipatanEmpat;
                    kelipatanEmpat += 4;
                    Console.Write(kelipatanEmpatArray[i] + " ");
                }
                Console.WriteLine();


                //Soal 5
                int soalLima = 1;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 3 == 0)
                    {
                        Console.Write("* ");
                    }
                    else
                    {
                        Console.Write(soalLima + " ");
                        soalLima += 4;
                    }
                }
                Console.WriteLine();


                //Soal 6
                int soalEnam = 1;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 3 == 0)
                    {
                        Console.Write("* ");
                    }
                    else
                    {
                        Console.Write((soalEnam) + " ");
                    }
                    soalEnam += 4;
                }
                Console.WriteLine();


                //Saol 7
                int soalTujuh = 2;
                for (int i = 0; i < length; i++)
                {
                    Console.Write(soalTujuh + " ");
                    soalTujuh *= 2;
                }
                Console.WriteLine();


                //Soal 8
                int soalDelapan = 3;
                for (int i = 0; i < length; i++)
                {
                    Console.Write(soalDelapan + " ");
                    soalDelapan *= 3;
                }
                Console.WriteLine();


                //Soal 9
                int soalSembilan = 4;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 3 == 0)
                    {
                        Console.Write("* ");
                    }
                    else
                    {
                        Console.Write(soalSembilan + " ");
                        soalSembilan *= 4;
                    }
                }
                Console.WriteLine();


                //Soal 10
                int soalSepuluh = 3;
                for (int i = 1; i <= length; i++)
                {
                    if (i % 4 == 0)
                    {
                        Console.Write("XXX ");
                    }
                    else
                    {
                        Console.Write(soalSepuluh + " ");
                    }
                    soalSepuluh *= 3;
                }
                Console.WriteLine();


                //Soal 11
                int[] fibonacciArray = new int[length];

                for (int i = 0; i < length; i++)
                {

                    if (i <= 1)
                    {
                        fibonacciArray[i] = 1;
                        Console.Write(fibonacciArray[i] + " ");
                    }
                    else
                    {
                        fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
                        Console.Write(fibonacciArray[i] + " ");
                    }
                }
                Console.WriteLine();


                //Soal 12
                int[] balikArray = new int[length];

                for (int i = 0; i < length; i++)
                {
                    if (length % 2 == 1)
                    {
                        if (i < 1) //ganjil
                        {
                            balikArray[i] = 1;
                            Console.Write(balikArray[i] + " ");
                        }
                        else if (i <= length / 2)
                        {
                            balikArray[i] = balikArray[i - 1] + 2;
                            Console.Write(balikArray[i] + " ");
                        }
                        else
                        {
                            balikArray[i] = balikArray[i - 1] - 2;
                            Console.Write(balikArray[i] + " ");
                        }
                    }

                    else
                    {
                        if (i < 1) //genap
                        {
                            balikArray[i] = 1;
                            Console.Write(balikArray[i] + " ");
                        }
                        else if (i < length / 2)
                        {
                            balikArray[i] = balikArray[i - 1] + 2;
                            Console.Write(balikArray[i] + " ");
                        }
                        else if (i == (length / 2))
                        {
                            balikArray[i] = balikArray[i - 1];
                            Console.Write(balikArray[i] + " ");
                        }
                        else
                        {
                            balikArray[i] = balikArray[i - 1] - 2;
                            Console.Write(balikArray[i] + " ");
                        }
                    }
                }
                Console.WriteLine();

                //Soal 12 Simple
                int[] duaBelasArray = new int[length];
                int kaloGanjil = 1;

                for (int i = 0; i < length; i++)
                {
                    if (length % 2 == 0)
                    {
                        if (i < length / 2)
                        {
                            duaBelasArray[i] = kaloGanjil;
                            duaBelasArray[length - 1 - i] = kaloGanjil;

                            kaloGanjil += 2;
                        }
                    }
                    else
                    {
                        if (i <= length / 2)
                        {
                            duaBelasArray[i] = kaloGanjil;
                            duaBelasArray[length - 1 - i] = kaloGanjil;

                            kaloGanjil += 2;
                        }
                    }
                }
                for (int i = 0; i < length; i++)
                {
                    Console.Write(duaBelasArray[i] + " ");
                }
                Console.WriteLine();


                //Soal 13
                int[] tigaBelasArray = new int[length];
                for (int i = 0; i < length; i++)
                {
                    if (i < 3)
                    {
                        tigaBelasArray[i] = 1;
                        Console.Write(tigaBelasArray[i] + " ");
                    }
                    else
                    {
                        tigaBelasArray[i] = tigaBelasArray[i - 1] + tigaBelasArray[i - 2] + tigaBelasArray[i - 3];
                        Console.Write(tigaBelasArray[i] + " ");
                    }
                }
                Console.WriteLine();


                //Soal 14
                //int[] empatBelasArray = new int[length];
                //int empatBelas = int.Parse(Console.ReadLine());
                //int j;
                //bool prima = true;

                //if (empatBelas >= 2)
                //{
                //    for (int i = 2; i <= empatBelas; i++)
                //    {
                //        for (empatBelasArray[i] = 2; empatBelasArray[i] < i; i++)
                //        {
                //            if ((i % empatBelasArray[i]) == 0)
                //            {
                //                prima = false;
                //                break;
                //            }
                //        }
                //        if (prima)
                //            Console.Write(empatBelasArray[i] + i + " ");
                //        prima = true;
                //    }
                //}
                //Console.WriteLine();


                //Soal 15 fibonacci bolak balik
                int[] limaBelasArray = new int[length];
                

                for (int i = 0; i < length; i++)
                {
                    if (length % 2 == 0)
                    {
                        if (i < length / 2)
                        {
                            limaBelasArray[i] = fibonacciArray[i];
                            limaBelasArray[length - 1 - i] = fibonacciArray[i];

                            //limaBelas += 2;
                        }
                    }
                    else
                    {
                        if (i <= length / 2)
                        {
                            limaBelasArray[i] = fibonacciArray[i];
                            limaBelasArray[length - 1 - i] = fibonacciArray[i];

                            //limaBelas += 2;
                        }
                    }
                }
                for (int i = 0; i < length; i++)
                {
                    
                    Console.Write(limaBelasArray[i] + " ");
                }
                Console.WriteLine();



            }
            else
            {
                Console.WriteLine("Nomor soal tidak ditemukan");
            }

            Console.ReadKey(); //ini buat pause console

        }
    }
}
