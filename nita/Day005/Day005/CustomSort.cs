﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day005
{
    class CustomSort
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan angka-angka yang ingin diurutkan");
            string numbers = Console.ReadLine();

            int[] numbersArray = Utility.ConvertStringToIntArray(numbers);



            int[] arr = { 78, 55, 45, 98, 13 };
            int temp;
            for (int j = 0; j <= numbersArray.Length - 2; j++)
            {
                for (int i = 0; i <= numbersArray.Length - 2; i++)
                {
                    if (numbersArray[i] > numbersArray[i + 1])
                    {
                        temp = numbersArray[i + 1];
                        numbersArray[i + 1] = numbersArray[i];
                        numbersArray[i] = temp;
                    }
                }
            }
            Console.WriteLine("Sorted:");
            foreach (int p in numbersArray)
                Console.Write(p + " ");
            Console.Read();
        }
    }
}
