﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day005
{
    class IntGeser
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan baris angka ");
            string numbers = Console.ReadLine();
            int[] arrayNumbers = Utility.ConvertStringToIntArray(numbers);

            Console.WriteLine("Masukkan banyaknya pergeseran");
            int angkaGeser = int.Parse(Console.ReadLine());

            for (int i = 0; i < angkaGeser; i++)
            {
                int elemenAkhir = arrayNumbers[arrayNumbers.Length - 1];

                for (int j = arrayNumbers.Length - 1; j > 0; j--)
                {
                    arrayNumbers[j] = arrayNumbers[j - 1];
                }
                arrayNumbers[0] = elemenAkhir;
            }

            for (int i = 0; i < arrayNumbers.Length; i++)
            {
                Console.Write(arrayNumbers[i] + " ");
            }
            Console.WriteLine();
        }
    }
}
