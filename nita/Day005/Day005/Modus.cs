﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day005
{
    class Modus
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan deret angka: ");
            string numbers = Console.ReadLine();
            int[] intArray = Utility.ConvertStringToIntArray(numbers);

            Array.Sort(intArray);

            int count = 0;
            int maksimum = 0;
            int temp = 0;

            string result = "";

            for (int i = 0; i < intArray.Length - 1; i++)
            {
                if (intArray[i] == intArray[i + 1])
                {
                    count++;
                }
                else
                {
                    if (count > maksimum)
                    {
                        maksimum = count;

                        result = intArray[i].ToString() + " ";

                    }
                    else if (count == maksimum)
                    {
                        result += intArray[i].ToString() + " ";
                    }
                    count = 0;
                }

            }
            if (count > maksimum)
            {
                maksimum = count;

                result = intArray[intArray.Length - 1].ToString() + " ";

            }
            else if (count == maksimum)
            {
                result += intArray[intArray.Length - 1].ToString() + " ";
            }

            Console.WriteLine(result);
        }
    }
}
