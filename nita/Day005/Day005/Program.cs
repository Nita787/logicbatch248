﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day005
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";
            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    case 2:
                        Console.WriteLine("Es Loli");
                        EsLoli.Resolve();
                        break;
                    case 3:
                        Console.WriteLine("Modus");
                        Modus.Resolve();
                        break;
                    case 4:
                        Console.WriteLine("Custom Sort");
                        CustomSort.Resolve();
                        break;
                    case 5:
                        Console.WriteLine("Pustaka");
                        Pustaka.Resolve();
                        break;

                    case 6:
                        Console.WriteLine("Kacamata Dan Baju");
                        KacamataDanBaju.Resolve();
                        break;
                    case 7:
                        Console.WriteLine("Lilin Fibonacci");
                        LilinFibonacci.Resolve();
                        break;
                    case 8:
                        Console.WriteLine("Int Geser");
                        IntGeser.Resolve();
                        break;
                    case 9:
                        Console.WriteLine("Parkir");
                        Parkir.Resolve();
                        break;
                    case 10:
                        Console.WriteLine("Naik Gunung");
                        NaikGunung.Resolve();
                        break;
                    case 11:
                        Console.WriteLine("Kaos Kaki");
                        KaosKaki.Resolve();
                        break;
                    case 12:
                        Console.WriteLine("Pembulatan Nilai");
                        PembulatanNilai.Resolve();
                        break;
                    
                }
                Console.WriteLine("Lanjutkan");
                answer = Console.ReadLine();
            }
        }
    }
}

