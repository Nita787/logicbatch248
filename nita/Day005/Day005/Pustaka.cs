﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day005
{
    class Pustaka
    {
        public static void Resolve()
        {
            Console.WriteLine("Berapa Buku yang dipinjam : ");
            int buku = int.Parse(Console.ReadLine());

            Console.WriteLine("Masukkan Tanggal Peminjaman : ");
            string timePeminjaman = Console.ReadLine();

            Console.WriteLine("Masukkan Tanggal Pengembalian : ");
            string timePengembalian = Console.ReadLine();

            DateTime datetime1 = Convert.ToDateTime(timePeminjaman);

            DateTime datetime2 = Convert.ToDateTime(timePengembalian);

            int lamaPeminjaman = (datetime2 - datetime1).Days;

            int totalDenda = (buku * lamaPeminjaman) * 5000;

            Console.WriteLine("Jumlah denda yang dibayarkan adalah Rp. " + totalDenda);

            Console.WriteLine();
        }
    }
}
