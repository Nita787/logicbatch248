﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class CaesarCipher
    {
        public static void Resolve()
        {
            int length, shift;

            Console.WriteLine("Masukkan length");
            length = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Masukkan kalimat");
            string input = Console.ReadLine();

            Console.WriteLine("Masukkan Rotasi");
            shift = Convert.ToInt32(Console.ReadLine());

            int[] alpha = new int[length];
            char[] rotate = new char[length]; //untuk nampung alfabet yg udah dirotate
            for (int i = 0; i < length; i++)
            {
                alpha[i] = (int)input[i];
                if (alpha[i] > 64 && alpha[i] < 91) //untuk huruf kapital
                {
                    alpha[i] = (65 + ((alpha[i] - 65) + shift) % 26);
                }
                else if (alpha[i] > 96 && alpha[i] < 123) //untuk huruf kecil
                {
                    alpha[i] = (97 + ((alpha[i] - 97) + shift) % 26);
                }
                rotate[i] = (char)alpha[i];
                Console.Write(rotate[i]);
            }
        }
    }
}
