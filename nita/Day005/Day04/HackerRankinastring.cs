﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class HackerRankinastring
    {
        public static void Resolve()
        {
            Console.WriteLine();

            Console.WriteLine("Masukkan jumlah kalimat : ");
            int jumlah = int.Parse(Console.ReadLine());
            string[] kalimatArray = new string[jumlah];

            string stringPembanding = "hackerrank";
            char[] charPembanding = stringPembanding.ToCharArray();

            //input kalimat berdasar jumlah inputan
            for (int i = 0; i < jumlah; i++)
            {
                Console.WriteLine("Masukkan kalimat ke - " + (i + 1));
                kalimatArray[i] = Console.ReadLine().ToLower();
            }

            //bandingkan dengan urutan karakter string pembanding "hackerrank" dengan urutan karakter kalimat yang diinput           
            for (int i = 0; i < kalimatArray.Length; i++)
            {
                //variabel nilai pembanding untuk mengeset nilai pembanding dengan awalan 0
                int jumlahPembanding = 0;
                char[] kalimatArrayChar = kalimatArray[i].ToCharArray();

                //perulangan untuk membandingkan urutan string kalimat inputan dengan urutan char pembanding "hackerrank" berdasar urutannya
                for (int j = 0; j < kalimatArrayChar.Length; j++)
                {
                    //kondisi pengecekan urutan string kalimat inputan dengan urutan char pembanding "hackerrank"
                    //apabila sesuai urutan terdapat kesamaan maka nilai pembanding akan bertambah
                    if (kalimatArrayChar[j] == charPembanding[jumlahPembanding])
                    {
                        jumlahPembanding++;
                    }
                }

                //karena jumlah karakter pembanding "hackerrank" = 10 maka jumlahnya harus 10
                //apabila jumlahnya "10" maka cetak output YES dan apabila jumlahnya selain "10" maka cetak output NO
                if (jumlahPembanding == charPembanding.Length)
                {
                    Console.WriteLine();
                    Console.WriteLine("Kalimat ke-" + (i + 1) + " adalah YES");
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("Kalimat ke-" + (i + 1) + " adalah NO");
                }
            }
        }
    }
}
