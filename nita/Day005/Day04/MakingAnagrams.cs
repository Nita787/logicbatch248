﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class MakingAnagrams
    {
        public static void Resolve()
        {
            Console.WriteLine("Enter the first set");
            string input1 = Console.ReadLine();

            Console.WriteLine("Enter the second set");
            string input2 = Console.ReadLine();

            char[] character1 = input1.ToLower().ToCharArray();
            char[] character2 = input2.ToLower().ToCharArray();

            int totalKarakter = input1.Length + input2.Length; //untuk menghitung banyak karakter dari kedua inputan

            for (int i = 0; i < input1.Length; i++)
            {
                for (int j = 0; j < input2.Length; j++)
                {
                    if (input1[i] == input2[j])
                    {
                        totalKarakter -= 2; //menghilangkan karakter yang sama
                    }
                }
            }
            Console.WriteLine(totalKarakter);
        }
    }
}
