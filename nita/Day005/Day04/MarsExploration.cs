﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class MarsExploration
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukan kalimat");
            string kalimat = Console.ReadLine();

            int hitungS = 0;
            int hitungO = 0;
            for (int i = 0; i < kalimat.Length; i++)
            {
                if (i % 3 == 0 || i % 3 == 2) 
                {
                    if (kalimat[i] != 'S')
                    {

                        hitungS++;
                    }
                }

                if (i % 3 == 1)
                {
                    if (kalimat[i] != 'O')
                    {

                        hitungO++;
                    }
                }
            }
            int hitungTotal = hitungS + hitungO;
            Console.WriteLine(hitungTotal);
        }
    }
}
