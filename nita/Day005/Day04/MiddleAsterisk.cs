﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class MiddleAsterisk
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan Kalimat");
            string asterisk = Console.ReadLine();


            string[] asteriskArray = asterisk.Split(' ');
            for (int i = 0; i < asteriskArray.Length; i++)
            {
                if (i < 3)
                {
                    Console.Write(asteriskArray[i].First() + " *** " + asteriskArray[i].Last() + " ");
                }
                else
                {
                    Console.WriteLine(i + " ");
                }
            }
        }
    }
}
