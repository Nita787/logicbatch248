﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Palindrome
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan Kalimat");
            string kalimat = Console.ReadLine();

            char[] kalimatChar = kalimat.ToCharArray();

            Array.Reverse(kalimatChar);
            string reverse = new string(kalimatChar);

            bool palindrome = kalimat.Equals(reverse, StringComparison.OrdinalIgnoreCase);

            if (palindrome == true)
            {
                Console.Write("yes");
            }
            else
            {
                Console.Write("No");
            }
            Console.WriteLine();
        }
    }
}
