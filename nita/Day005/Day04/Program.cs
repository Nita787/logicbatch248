﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class Program
    {
        static void Main(string[] args)
        {
            string answer = "Y";
            while (answer.ToUpper() == "Y")
            {
                Console.WriteLine("Masukkan nomor soal");
                int nomorSoal = int.Parse(Console.ReadLine());

                switch (nomorSoal)
                {
                    
                    case 1:
                        Console.WriteLine("Camel Case");
                        CamelCase.Resolve();
                        break;
                    case 2:
                        Console.WriteLine("Strong Password");
                        StrongPassword.Resolve();
                        break;
                    case 3:
                        Console.WriteLine("Caesar Cipher");
                        CaesarCipher.Resolve();
                        break;
                    case 4:
                        Console.WriteLine("Mars Exploration");
                        MarsExploration.Resolve();
                        break;
                    case 5:
                        Console.WriteLine("HackerRank in a string");
                        HackerRankinastring.Resolve();
                        break;
                    case 6:
                        Console.WriteLine("Pangrams");
                        Pangrams.Resolve();
                        break;
                    case 7:
                        Console.WriteLine("Separate The Numbers");
                        SeparatetheNumbers.Resolve();
                        break;
                    case 8:
                        Console.WriteLine("Gemstone");
                        GemStone.Resolve();
                        break;
                    case 9:
                        Console.WriteLine("Making Anagrams");
                        MakingAnagrams.Resolve();
                        break;
                    case 10:
                        Console.WriteLine(" Two Strings");
                        TwoStrings.Resolve();
                        break;
                    case 11:
                        Console.WriteLine("Middle Asterisk");
                        MiddleAsterisk.Resolve();
                        break;
                    case 12:
                        Console.WriteLine("Palindrome");
                        Palindrome.Resolve();
                        break;


                    
                }
                Console.WriteLine("Lanjutkan");
                answer = Console.ReadLine();
            }
        }
    }
}
