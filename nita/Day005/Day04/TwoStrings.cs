﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04
{
    class TwoStrings
    {
        public static void Resolve()
        {
            Console.WriteLine("Masukkan kalimat:");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Masukkan kalimat:");
            string kalimat2 = Console.ReadLine();

            char[] kalimatChar = kalimat.ToCharArray();
            char[] kalimat2Char = kalimat2.ToCharArray();

            Boolean condition = false;

            for (int i = 0; i < kalimatChar.Length; i++)
            {
                for (int j = 0; j < kalimat2Char.Length; j++)
                {
                    if (kalimatChar[i].Equals(kalimat2Char[j]))
                    {
                        condition = true;
                        break;
                    }
                }
            }

            if (condition)
            {
                Console.WriteLine("YES");
            }
            else
            {
                Console.WriteLine("NO");
            }

        }
    }
}
